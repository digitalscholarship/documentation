### README ###

**Documentation** will contain technical documents for various projects. Documents are authored using an open source word processor called *LibreOffice* (.odt) and exported in PDF, .docx, and .doc formats. 

[Download Documents](https://bitbucket.org/digitalscholarship/documentation/src/0d45e95a99e6aa490a4c6c659f7182390b3ee41a?at=master)

Feedback, comments, concerns, questions? [Henry Le](mailto:hutle@ucdavis.edu)